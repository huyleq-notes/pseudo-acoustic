\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper,margin=2cm}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{tcolorbox}

% Syntax: \colorboxed[<color model>]{<color specification>}{<math formula>}
\newcommand*{\colorboxed}{}
\def\colorboxed#1#{%
  \colorboxedAux{#1}%
}
\newcommand*{\colorboxedAux}[3]{%
  % #1: optional argument for color model
  % #2: color specification
  % #3: formula
  \begingroup
  \colorlet{cb@saved}{.}%
  \color#1{#2}%
  \boxed{%
  \color{cb@saved}%
  #3%
  }%
\endgroup
}


\newcommand{\xx}{\mathbf{x}}

\title{Instability in Adjoint Pseudo-Acoustic Anisotropic Wave Equations}
\author{Huy Le}
%\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle

\section{Introduction}
Define functional inner product as 
\begin{equation}
\langle u,v \rangle = \int_0^T\int_{\Omega}uv d\xx dt.
\end{equation}
Adjoint operator is defined by
\begin{equation}
\langle u,Lv \rangle = \langle L^{*}u,v \rangle.
\end{equation}
For a particular form of $L=\partial_x^2$
\begin{align}
& \langle u,Lv \rangle = \langle u,\partial_x^2v \rangle = \int_0^T\int_{\Omega}u\partial_x^2v d\xx dt\\
& = \int_0^T\int_{\Omega_{yz}}u\partial_xv dydzdt |_{\Omega_x} - \int_0^T\int_{\Omega_{yz}}v\partial_x udydzdt |_{\Omega_x} +  \int_0^T\int_{\Omega}v\partial_x^2u d\xx dt\\
& = \int_0^T\int_{\Omega}v\partial_x^2u d\xx dt=\langle \partial_x^2u,v \rangle.
\end{align}
where integration by parts has been carried out twice in the $x$-direction and boundary conditions $u|_{\Omega_x}=v|_{\Omega_x}=0$. So by definition, $L=\partial_x^2$ is self-adjoint $L^{*}=\partial_x^2$.
\par
Similarly, for $L=c\partial_x^2$, where $c$ can be velocity or any medium parameter,
\begin{equation}
\langle u,Lv \rangle = \langle u,c\partial_x^2v \rangle = \langle cu,\partial_x^2v \rangle = \langle \partial_x^2cu,v \rangle.
\end{equation}
As a result, this operator is not self-adjoint $L^*=\partial_x^2c$.
\par
One can easily find the adjoints of the following common differential operators

\begin{center}
\begin{tabular}{ |c c c| } 
 \hline
 $L$ & $L^*$ & Self-adjoint \\ 
 \hline
 $\partial_i^2$ & $\partial_i^2$ & yes \\ 
 \hline
 $c\partial_i^2$ & $\partial_i^2c$ & no \\ 
 \hline
 $a\partial_i^2b$ & $b\partial_i^2a$ & no \\ 
 \hline
 $\partial_i$ & $-\partial_i$ & no \\ 
 \hline
 $a\partial_ib$ & $-b\partial_ia$ & no \\ 
 \hline
 $\partial_ic\partial_j$ & $\partial_jc\partial_i$ & no \\ 
 \hline
\end{tabular}
\end{center}

\section{Non-self-adjoint system}
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=c_{11}(\partial_x^2+\partial_y^2)\sigma_x+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=c_{13}(\partial_x^2+\partial_y^2)\sigma_x+c_{33}\partial_z^2\sigma_z,
\end{cases}
\label{eq:forward0}
\end{equation}
which can be written in matrix form as
\begin{equation}
\partial_t^2\sigma=CD\sigma,
\label{eq:forward1}
\end{equation}
where
\begin{equation}
\sigma=\begin{bmatrix} \sigma_x \\ \sigma_z \end{bmatrix},C=\begin{bmatrix} c_{11} & c_{13} \\ c_{13} & c_{33} \end{bmatrix},D=\begin{bmatrix} \partial_x^2+\partial_y^2 &  \\  & \partial_z^2 \end{bmatrix},
\end{equation}
with
\begin{align}
c_{11}&=v^2(1+2\epsilon)=v_x^2,\\
c_{13}&=v^2\sqrt{1+2\delta}=vv_n,\\
c_{33}&=v^2.
\end{align}
\par
The adjoint equations is
\begin{equation}
\partial_t^2\lambda=DC\lambda,
\label{eq:adjoint1}
\end{equation}
or
\begin{equation}
\begin{cases}
\partial_t^2\lambda_x=(\partial_x^2+\partial_y^2)(c_{11}\lambda_x+c_{13}\lambda_z),\\
\partial_t^2\lambda_z=\partial_z^2(c_{13}\lambda_x+c_{33}\lambda_z),
\end{cases}
\end{equation}
where $\lambda=\begin{bmatrix} \lambda_x \\ \lambda_z \end{bmatrix}$ is the adjoint wave fields.
\par
The gradients of the objective function $\chi$ with respect to $c_{ij}$ are
\begin{align}
\frac{\partial\chi}{\partial c_{11}}&=\int_0^T\lambda_x(\partial_x^2+\partial_y^2)\sigma_xdt,\\
\frac{\partial\chi}{\partial c_{13}}&=\int_0^T\lambda_x\partial_z^2\sigma_z+\lambda_z(\partial_x^2+\partial_y^2)\sigma_xdt,\\
\frac{\partial\chi}{\partial c_{33}}&=\int_0^T\lambda_z\partial_z^2\sigma_zdt,
\end{align}
from which gradients with respect to $(v,\epsilon,\delta)$ can be computed from chain rule
\begin{align}
\frac{\partial\chi}{\partial v}&=\frac{\partial\chi}{\partial c_{11}}2v(1+2\epsilon)+\frac{\partial\chi}{\partial c_{13}}2v\sqrt{1+2\delta}+\frac{\partial\chi}{\partial c_{33}}2v,\label{eq:dv}\\
\frac{\partial\chi}{\partial \epsilon}&=\frac{\partial\chi}{\partial c_{11}}2v^2,\label{eq:deps}\\
\frac{\partial\chi}{\partial \delta}&=\frac{\partial\chi}{\partial c_{13}}\frac{v^2}{\sqrt{1+2\delta}}\label{eq:ddel}.
\end{align}
\par
Unfortunately, the above adjoint equations have solutions that grow linearly with time. In fact, for isotropic media, $c_{11}=c_{13}=c_{33}$, if $(\lambda_x,\lambda_z)$ is a solution, so is $(\lambda_x+t,\lambda_z-t)$. 

\section{Self-adjoint system}
One way to make the system self-adjoint is to define
\begin{equation}
R=\sqrt{C}=\begin{bmatrix} r_{11} & r_{13} \\ r_{13} & r_{33} \end{bmatrix},
\end{equation}
and rewrite the system as
\begin{equation}
\partial_t^2\sigma=RDR\sigma,
\label{eq:forward2}
\end{equation}
or
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=r_{11}(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+r_{13}\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z),\\
\partial_t^2\sigma_z=r_{13}(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+r_{33}\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z).
\end{cases}
\end{equation}
Because $C$ is always symmetric and positive semi-definite when $\epsilon\ge\delta$, $R$ exists and is also symmetric and positive semi-definite. 
\par
Equation \ref{eq:forward2} correctly captures the kinematics of the original equation \ref{eq:forward1}. It can be shown that the two systems have the same dispersion relation
\begin{equation}
\det(C\hat{D}+\omega^2I)=\det(R\hat{D}R+\omega^2I),
\end{equation}
where
\begin{equation}
\hat{D}=-\begin{bmatrix} k_x^2+k_y^2 &  \\  & k_z^2 \end{bmatrix}.
\end{equation}
Moreover, if $C$ is nonsingular, system \ref{eq:forward1} is equivalent to system \ref{eq:forward2} by a change of variable $\sigma\rightarrow R\sigma$. In fact, the adjoint system \ref{eq:adjoint1} is also equivalent to system \ref{eq:forward2} by a similar change of variable $\lambda\rightarrow R^{-1}\sigma$.
\par
The gradients of the objective function with respect to $r_{ij}$ are
\begin{align}
\frac{\partial\chi}{\partial r_{11}}={}&\int_0^T\left[\lambda_x(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+\sigma_x(\partial_x^2+\partial_y^2)(r_{11}\lambda_x+r_{13}\lambda_z)\right]dt,\\
\begin{split}
\frac{\partial\chi}{\partial r_{13}}={}&\int_0^T\left[\lambda_x\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z)+\lambda_z(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+\right.\\&\left.\sigma_x\partial_z^2(r_{13}\lambda_x+r_{33}\lambda_z)+\sigma_z(\partial_x^2+\partial_y^2)(r_{11}\lambda_x+r_{13}\lambda_z)\right]dt,
\end{split}\\
\frac{\partial\chi}{\partial r_{33}}={}&\int_0^T\left[\lambda_z\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z)+\sigma_z\partial_z^2(r_{13}\lambda_x+r_{33}\lambda_z)\right]dt.
\end{align}
\par
Elements of $R$ are computed from trace and determinant of $C$
\begin{align}
r_{11}&=\frac{v}{t}(1+2\epsilon+s),\\
r_{13}&=\frac{v}{t}\sqrt{1+2\delta},\\
r_{33}&=\frac{v}{t}(1+s),
\end{align}
where
\begin{equation}
s=\sqrt{2(\epsilon-\delta)},t=\sqrt{2(\epsilon+1)+2s}.
\end{equation}
Since $s=\sqrt{2(\epsilon-\delta)}$ can be zero, derivatives of $s$ with respect to either $\epsilon$ or $\delta$ can become unbounded. As a result, one has to define new variables
\begin{align}
\alpha=&1+s,\\
\beta=&\sqrt{1+2\delta}.
\end{align}
so that
\begin{align}
r_{11}&=\frac{v}{t}(\beta^2+\alpha^2-\alpha),\\
r_{13}&=\frac{v\beta}{t},\\
r_{33}&=\frac{v\alpha}{t},
\end{align}
with 
\begin{equation}
t=\sqrt{\alpha^2+\beta^2}.
\end{equation}
Now the gradients with respect to $(v,\alpha,\beta)$ can be computed from change rule
\begin{align}
\frac{\partial\chi}{\partial v}&=\frac{\partial\chi}{\partial r_{11}}\frac{\beta^2+\alpha^2-\alpha}{t}+\frac{\partial\chi}{\partial r_{13}}\frac{\beta}{t}+\frac{\partial\chi}{\partial r_{33}}\frac{\alpha}{t},\\
\frac{\partial\chi}{\partial \alpha}&=\frac{\partial\chi}{\partial r_{11}}\frac{v(\alpha^3+\alpha\beta^2-\beta^3)}{t^3}-\frac{\partial\chi}{\partial r_{13}}\frac{v\alpha\beta}{t^3}+\frac{\partial\chi}{\partial r_{33}}\frac{v\beta^2}{t^3},\\
\frac{\partial\chi}{\partial \beta}&=\frac{\partial\chi}{\partial r_{11}}\frac{v\beta(\alpha^2+\alpha+\beta^2)}{t^3}+\frac{\partial\chi}{\partial r_{13}}\frac{v\alpha^2}{t^3}-\frac{\partial\chi}{\partial r_{33}}\frac{v\alpha\beta}{t^3}.
\end{align}
After the inversion, $(\epsilon,\delta)$ can be obtained by
\begin{align}
\delta&=\frac{\beta^2-1}{2},\\
\epsilon&=\frac{(\alpha-1)^2}{2}+\delta.
\end{align}
Overall, it requires 10 three-dimensional arrays to implement this system: $(\sigma_x,\sigma_z)$, $(\lambda_x,\lambda_z)$, $(r_{ij})$, and $(v,\alpha,\beta)$ (not counting arrays for the gradients). 

\section{Another self-adjoint system}
Another way to make system \ref{eq:forward1} self-adjoint is to split the differential operator $D$ instead of the medium matrix $C$. However, to avoid taking the square root of the horizontal derivatives $\partial_x^2+\partial_y^2$, which would result in a pseudo-differential operator, rewrite the system with three equations
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=c_{11}\partial_x^2\sigma_x+c_{11}\partial_y^2\sigma_y+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_y=c_{11}\partial_x^2\sigma_x+c_{11}\partial_y^2\sigma_y+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=c_{13}\partial_x^2\sigma_x+c_{13}\partial_y^2\sigma_y+c_{33}\partial_z^2\sigma_z,
\end{cases}
\end{equation}
which has the same matrix form as system \ref{eq:forward1}
\begin{equation}
\partial_t^2\sigma=CD\sigma,
\label{eq:forward3}
\end{equation}
but with
\begin{equation}
\sigma=\begin{bmatrix} \sigma_x \\ \sigma_y \\ \sigma_z \end{bmatrix},C=\begin{bmatrix} c_{11} & c_{11} & c_{13} \\ c_{11} & c_{11} & c_{13} \\ c_{13} & c_{13} & c_{33} \end{bmatrix},D=\begin{bmatrix} \partial_x^2 & & \\ & \partial_y^2 & \\  & & \partial_z^2 \end{bmatrix}.
\end{equation}
As a result, the adjoint system has the same form as equations \ref{eq:adjoint1}
\begin{equation}
\partial_t^2\lambda=DC\lambda,
\label{eq:adjoint3}
\end{equation}
or
\begin{equation}
\begin{cases}
\partial_t^2\lambda_x=\partial_x^2(c_{11}\lambda_x+c_{11}\lambda_y+c_{13}\lambda_z),\\
\partial_t^2\lambda_y=\partial_y^2(c_{11}\lambda_x+c_{11}\lambda_y+c_{13}\lambda_z),\\
\partial_t^2\lambda_z=\partial_z^2(c_{13}\lambda_x+c_{13}\lambda_y+c_{33}\lambda_z).
\end{cases}
\end{equation}
This adjoint system suffers the same instability as system \ref{eq:adjoint1}.
\par
Now to make it self-adjoint, define 
\begin{equation}
D_1=\sqrt{D}=\begin{bmatrix} \partial_x & & \\ & \partial_y & \\  & & \partial_z \end{bmatrix},
\end{equation}
and rewrite the system \ref{eq:forward3} as
\begin{equation}
\partial_t^2\sigma=D_1CD_1\sigma,
\label{eq:forward4}
\end{equation}
or
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\partial_x(c_{11}\partial_x\sigma_x+c_{11}\partial_y\sigma_y+c_{13}\partial_z\sigma_z),\\
\partial_t^2\sigma_y=\partial_y(c_{11}\partial_x\sigma_x+c_{11}\partial_y\sigma_y+c_{13}\partial_z\sigma_z),\\
\partial_t^2\sigma_z=\partial_z(c_{13}\partial_x\sigma_x+c_{13}\partial_y\sigma_y+c_{33}\partial_z\sigma_z).
\end{cases}
\end{equation}
It can be verified that systems \ref{eq:forward3} and \ref{eq:forward4} have the same dispersion relation, i.e. describing the same wave kinematics
\begin{equation}
\det(C\hat{D}+\omega^2I)=\det(\hat{D_1}C\hat{D_1}+\omega^2I),
\end{equation}
where
\begin{equation}
\hat{D}=-\begin{bmatrix} k_x^2 & & \\ & k_y^2 & \\ & & k_z^2 \end{bmatrix},\hat{D_1}=\begin{bmatrix} ik_x & & \\ & ik_y & \\ & & ik_z \end{bmatrix}.
\end{equation}
In fact, if one applies $D_1$ on both sides of system \ref{eq:forward3} and make a change of variables $\sigma'=D_1\sigma$, they would arrive at system \ref{eq:forward4}. This means that if $(\sigma_x,\sigma_x,\sigma_z)$ is a smooth solution of system \ref{eq:forward3}, $(\partial_x\sigma_x,\partial_y\sigma_x,\partial_z\sigma_z)$ is a solution of system \ref{eq:forward4}. 
\par
The gradients of the objective function with respect to $c_{ij}$ now are
\begin{align}
\frac{\partial\chi}{\partial c_{11}}&=\int_0^T(\partial_x\lambda_x+\partial_y\lambda_y)(\partial_x\sigma_x+\partial_y\sigma_y)dt,\\
\frac{\partial\chi}{\partial c_{13}}&=\int_0^T(\partial_x\lambda_x+\partial_y\lambda_y)\partial_z\sigma_z+(\partial_x\sigma_x+\partial_y\sigma_y)\partial_z\lambda_zdt,\\
\frac{\partial\chi}{\partial c_{33}}&=\int_0^T\partial_z\lambda_z\partial_z\sigma_zdt,
\end{align}
from which gradients with respect to $(v,\epsilon,\delta)$ can be computed from chain rule as before (equations \ref{eq:dv}, \ref{eq:deps}, and \ref{eq:ddel}).
\par
Compared to system \ref{eq:forward2}, system \ref{eq:forward4} requires 15 three-dimensional arrays to implement: $(\sigma_x,\sigma_y,\sigma_z)$, $(\lambda_x,\lambda_y,\lambda_z)$, $(\partial_x\sigma_x,\partial_y\sigma_y,\partial_z\sigma_z)$, $(\partial_x\lambda_x,\partial_y\lambda_y,\partial_z\lambda_z)$, and $(v,\epsilon,\delta)$. Additionally, staggered grids have to be used (Figure \ref{fig:grid}). However, the amount of computation is expected to be less for system \ref{eq:forward4} than for system \ref{eq:forward1} due to simpler gradient formulas. If one factorizes $\partial_x^2+\partial_y^2$ instead of rewriting system \ref{eq:forward1} with three equations (system \ref{eq:forward3}), they would need only 11 arrays, but then have to implement in Fourier domain or use helical coordinates and spectral fractorization. Formulation for that system is similar to system \ref{eq:forward4}.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{grid}
\caption{Staggered grids for system \ref{eq:forward4}.}
\label{fig:grid}
\end{figure}

\par
Numerial experiments show that even though system \ref{eq:forward4} is self-adjoint, it is unstable. Indeed, if one integrates system \ref{eq:adjoint3}, which is unstable, it proves that $(\sigma_x,\sigma_y,\sigma_z)=(\int_x\lambda_x,\int_y\lambda_y,\int_z\lambda_z)$ is a solution of system \ref{eq:forward4}.

\section{Non-self-adjoint but stable system}
It seems that systems that have all spatial derivatives in at least one of two equations are stable. The general form of such system is
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[a_1(\partial_x^2+\partial_y^2)+a_2\partial_z^2\right]\sigma_x+\left[b_1(\partial_x^2+\partial_y^2)+b_2\partial_z^2\right]\sigma_z,\\
\partial_t^2\sigma_z=\left[c_1(\partial_x^2+\partial_y^2)+c_2\partial_z^2\right]\sigma_x+\left[d_1(\partial_x^2+\partial_y^2)+d_2\partial_z^2\right]\sigma_z,
\end{cases}
\label{eq:forward5}
\end{equation}
or
\begin{equation}
\partial_t^2\sigma=\begin{bmatrix} A & B \\ C & D \end{bmatrix}\sigma,
\end{equation}
where $A=a_1(\partial_x^2+\partial_y^2)+a_2\partial_z^2$ and so on.
The adjoint equations are
\begin{equation}
\partial_t^2\lambda=\begin{bmatrix} A^T & C^T \\ B^T & D^T \end{bmatrix}\lambda,
\end{equation}
or
\begin{equation}
\begin{cases}
\partial_t^2\lambda_x=(\partial_x^2+\partial_y^2)(a_1\lambda_x+c_1\lambda_z)+\partial_z^2(a_2\lambda_x+c_2\lambda_z),\\
\partial_t^2\lambda_z=(\partial_x^2+\partial_y^2)(b_1\lambda_x+d_1\lambda_z)+\partial_z^2(b_2\lambda_x+d_2\lambda_z).
\end{cases}
\label{eq:adjoint5}
\end{equation}
\par
Derivatives with respect to these coefficients $(a_i,b_i,c_i,d_i)$ are
\begin{align}
\frac{\partial\chi}{\partial a_1}&=\int_0^T\lambda_x(\partial_x^2+\partial_y^2)\sigma_xdt,\\
\frac{\partial\chi}{\partial a_2}&=\int_0^T\lambda_x\partial_z^2\sigma_xdt,\\
\frac{\partial\chi}{\partial b_1}&=\int_0^T\lambda_x(\partial_x^2+\partial_y^2)\sigma_zdt,\\
\frac{\partial\chi}{\partial b_2}&=\int_0^T\lambda_x\partial_z^2\sigma_zdt,\\
\frac{\partial\chi}{\partial c_1}&=\int_0^T\lambda_z(\partial_x^2+\partial_y^2)\sigma_xdt,\\
\frac{\partial\chi}{\partial c_2}&=\int_0^T\lambda_z\partial_z^2\sigma_xdt,\\
\frac{\partial\chi}{\partial d_1}&=\int_0^T\lambda_z(\partial_x^2+\partial_y^2)\sigma_zdt,\\
\frac{\partial\chi}{\partial d_2}&=\int_0^T\lambda_z\partial_z^2\sigma_zdt.
\end{align}
\par
For the this system to correctly describe the kinematics, its dispersion relation has to be equal to that of system \ref{eq:forward1}, which results in five constraints on eight medium parameters $(a_i,b_i,c_i,d_i)$
\begin{align}
a_1+d_1&=c_{11},\label{eq:abcd1}\\
a_2+d_2&=c_{33},\label{eq:abcd2}\\
a_1d_1-b_1c_1&=0,\label{eq:abcd3}\\
a_2d_2-b_2c_2&=0,\label{eq:abcd4}\\
a_1d_2+a_2d_1-b_1c_2-b_2c_1&=c_{11}c_{33}-c_{13}^2.\label{eq:abcd5}
\end{align}
These constraints form an under-determined system. There are multiple equivalent solutions. Because of equations \ref{eq:abcd3} and \ref{eq:abcd4}, if one of these coefficients is zero, at least one other must also vanish. I will first consider this case and save the case in which all coefficients are non-zero for last.

\subsection{Case 1: $a_1=b_1=0$}
System \ref{eq:forward5} becomes
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=a_2\partial_z^2\sigma_x+b_2\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=\left[c_1(\partial_x^2+\partial_y^2)+c_2\partial_z^2\right]\sigma_x+\left[d_1(\partial_x^2+\partial_y^2)+d_2\partial_z^2\right]\sigma_z.
\end{cases}
\label{eq:forward51}
\end{equation}

\subsubsection{$a_2b_2\neq0$ and $c_2d_2\neq0$}
Because of equation \ref{eq:abcd4}, define $r=\frac{a_2}{c_2}=\frac{b_2}{d_2}$. Because $r\neq0$, multiply the second equation in \ref{eq:forward51} by $r$ and subtract the first equation, resulting in an equivalent system
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=a_2\partial_z^2\sigma_x+b_2\partial_z^2\sigma_z,\\
\partial_t^2(r\sigma_z-\sigma_x)=rc_1(\partial_x^2+\partial_y^2)\sigma_x+rd_1(\partial_x^2+\partial_y^2)\sigma_z,
\end{cases}
\label{eq:forward511}
\end{equation}
which, after a change of variable $\sigma_z'=r\sigma_z-\sigma_x$ or $\sigma_z=\frac{\sigma_z'+\sigma_x}{r}$, becomes
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=(a_2+\frac{b_2}{r})\partial_z^2\sigma_x+\frac{b_2}{r}\partial_z^2\sigma_z',\\
\partial_t^2\sigma_z'=(rc_1+d_1)(\partial_x^2+\partial_y^2)\sigma_x+d_1(\partial_x^2+\partial_y^2)\sigma_z'.
\end{cases}
\label{eq:forward512}
\end{equation}
This system is a special case of the general system \ref{eq:forward5} with $a_1=b_1=c_2=d_2=0$. 

\subsubsection{$a_2b_2\neq0$ and $c_2d_2=0$}
Constraint \ref{eq:abcd4} dictates that $c_2=d_2=0$, which leads us back to the above case of $a_1=b_1=c_2=d_2=0$.

\subsubsection{$a_2b_2=0$ and $c_2d_2\neq0$}
In this case, $a_2=b_2=0$, resulting in no solution because constraint \ref{eq:abcd5} is violated.

\subsubsection{$a_2b_2=c_2d_2=0$}
This results in four cases
\begin{itemize}
\item $a_2=c_2=0$: this case has a solution.
\item $a_2=d_2=0$: violation of constraint \ref{eq:abcd2}.
\item $b_2=c_2=0$: the five constraints reduce to
\begin{align}
d_1&=c_{11},\\
a_2+d_2&=c_{33},\\
a_2d_2&=0,\\
a_2d_1&=c_{11}c_{33}-c_{13}^2.
\end{align}
The last constraint requires that $a_2\neq0$, so $d_2=0$, which, means $a_2=c_{33}$. This does not satisfy the last constraints.
\item $b_2=d_2=0$: the five constraints reduce to
\begin{align}
d_1&=c_{11},\\
a_2&=c_{33},\\
a_2d_1&=c_{11}c_{33}-c_{13}^2,
\end{align}
in which the last constraint is not satisfied.
\end{itemize}
\par
In conclusion, for the case $a_1=b_1=0$, there are two solutions $a_1=b_1=c_2=d_2=0$ and $a_1=b_1=a_2=c_2=0$. The first solution results in
\begin{align}
a_2&=c_{33},\\
d_1&=c_{11},\\
b_2c_1&=c_{13}^2.
\end{align}
If one chooses $b_2=c_1=c_{13}$ for example, one gets the system 
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=c_{33}\partial_z^2\sigma_x+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=c_{13}(\partial_x^2+\partial_y^2)\sigma_x+c_{11}(\partial_x^2+\partial_y^2)\sigma_z.
\end{cases}
\label{eq:forward513}
\end{equation}
The second solution results in
\begin{align}
d_1&=c_{11},\\
d_2&=c_{33},\\
b_2c_1&=c_{13}^2-c_{11}c_{33}=v^4(\delta-\epsilon).
\end{align}
If one chooses $b_2=v^2(\delta-\epsilon)$ and $c_1=v^2$ for example, one gets the system 
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=v^2(\delta-\epsilon)\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=v^2(\partial_x^2+\partial_y^2)\sigma_x+\left[v^2(1+2\epsilon)(\partial_x^2+\partial_y^2)+v^2\partial_z^2\right]\sigma_z.
\end{cases}
\label{eq:forward514}
\end{equation}
Exchanging $b_2$ and $c_1$ results in 
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=v^2\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=v^2(\delta-\epsilon)(\partial_x^2+\partial_y^2)\sigma_x+\left[v^2(1+2\epsilon)(\partial_x^2+\partial_y^2)+v^2\partial_z^2\right]\sigma_z.
\end{cases}
\label{eq:forward515}
\end{equation}


\subsection{Case 2: $a_1=c_1=0$}
This case is similar to the case $b_1=d_1=0$, considered below, if one interchanges $\sigma_x\leftrightarrow\sigma_z$.

\subsection{Case 3: $b_1=d_1=0$}
System \ref{eq:forward5} becomes
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[a_1(\partial_x^2+\partial_y^2)+a_2\partial_z^2\right]\sigma_x+b_2\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=\left[c_1(\partial_x^2+\partial_y^2)+c_2\partial_z^2\right]\sigma_x+d_2\partial_z^2\sigma_z,
\end{cases}
\label{eq:forward52}
\end{equation}
Similar analysis as in the first case applies. 

\subsubsection{$a_2b_2\neq0$ and $c_2d_2\neq0$}
Define $r=\frac{a_2}{c_2}=\frac{b_2}{d_2}$, multiply the second equation in \ref{eq:forward52} by $r$, and subtract from the first equation
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[a_1(\partial_x^2+\partial_y^2)+a_2\partial_z^2\right]\sigma_x+b_2\partial_z^2\sigma_z,\\
\partial_t^2(\sigma_x-r\sigma_z)=(a_1-rc_1)(\partial_x^2+\partial_y^2)\sigma_x,
\end{cases}
\label{eq:forward521}
\end{equation}
which, after a change of variable $\sigma_z'=\sigma_x-r\sigma_z$ or $\sigma_z=\frac{\sigma_x-\sigma_z'}{r}$, becomes
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[a_1(\partial_x^2+\partial_y^2)+(a_2+\frac{b_2}{r})\partial_z^2\right]\sigma_x-\frac{b_2}{r}\partial_z^2\sigma_z',\\
\partial_t^2\sigma_z'=(a_1-rc_1)(\partial_x^2+\partial_y^2)\sigma_x.
\end{cases}
\label{eq:forward522}
\end{equation}
This system is a special case of the general system \ref{eq:forward5} with $b_1=d_1=c_2=d_2=0$. 

\subsubsection{$a_2b_2\neq0$ and $c_2d_2=0$}
In this case, $c_2=d_2=0$, which leads us back to the above case of $b_1=d_1=c_2=d_2=0$.

\subsubsection{$a_2b_2=0$ and $c_2d_2\neq0$}
In this case, $a_2=b_2=0$, the five constraints reduce to
\begin{align}
a_1&=c_{11},\\
d_2&=c_{33},\\
a_1d_2&=c_{11}c_{33}-c_{13}^2.
\end{align}
There is no solution because the last constraint is not satisfied.

\subsubsection{$a_2b_2=c_2d_2=0$}
Consider four cases
\begin{itemize}
\item $a_2=c_2=0$: this case has a solution.
\item $a_2=d_2=0$: violation of constraint \ref{eq:abcd2}.
\item $b_2=c_2=0$: the five constraints reduce to
\begin{align}
a_1&=c_{11},\\
a_2+d_2&=c_{33},\\
a_2d_2&=0,\\
a_1d_2&=c_{11}c_{33}-c_{13}^2.
\end{align}
The last constraint requires that $d_2\neq0$, so $a_2=0$, which, means $d_2=c_{33}$. This does not satisfy the last constraints.
\item $b_2=d_2=0$: violation of constraint \ref{eq:abcd5}.
\end{itemize}
\par
In conclusion, for the case $b_1=d_1=0$, there are two solutions $b_1=d_1=c_2=d_2=0$ and $b_1=d_1=a_2=c_2=0$. The first solution results in
\begin{align}
a_1&=c_{11},\\
a_2&=c_{33},\\
b_2c_1&=c_{13}^2-c_{11}c_{33}=v^4(\delta-\epsilon).
\end{align}
If one chooses $b_2=v^2$ and $c_1=v^2(\delta-\epsilon)$ for example, one gets the system 
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[v^2(1+2\epsilon)(\partial_x^2+\partial_y^2)+v^2\partial_z^2\right]\sigma_x+v^2\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=v^2(\delta-\epsilon)(\partial_x^2+\partial_y^2)\sigma_x,
\end{cases}
\label{eq:forward523}
\end{equation}
Exchanging $b_2$ and $c_1$ gives 
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=\left[v^2(1+2\epsilon)(\partial_x^2+\partial_y^2)+v^2\partial_z^2\right]\sigma_x+v^2(\delta-\epsilon)\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=v^2(\partial_x^2+\partial_y^2)\sigma_x,
\end{cases}
\label{eq:forward524}
\end{equation}
The second solution results in
\begin{align}
a_1&=c_{11},\\
d_2&=c_{33},\\
b_2c_1&=c_{13}^2.
\end{align}
If one chooses $b_2=c_1=c_{13}$ for example, one gets back the system \ref{eq:forward0}
\begin{equation}
\begin{cases}
\partial_t^2\sigma_x=c_{11}(\partial_x^2+\partial_y^2)\sigma_x+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=c_{13}(\partial_x^2+\partial_y^2)\sigma_x+c_{33}\partial_z^2\sigma_z.
\end{cases}
\label{eq:forward525}
\end{equation}

\subsection{Case 4: $c_1=d_1=0$}
This case is similar to the case $a_1=b_1=0$, already considered above, if one interchanges $\sigma_x\leftrightarrow\sigma_z$.
\par
The above four cases consider what happens when one of four coefficients $(a_1,b_1,c_1,d_1)$ is zero. Analysis of four other cases when one of $(a_2,b_2,c_2,d_2)$ becomes zero is similar by just interchanging $(\partial_x^2+\partial_y^2)\leftrightarrow\partial_z^2$. Unfortunately, none of the above analyzed solutions produces stable forward and adjoint systems. The only stable solution is one with all non-zero coefficients.

\subsection{Case 5: non-zero coefficients}
There are still multiple solutions with all non-zero coefficients. To simplify algebra and latter computation, one choice is to have symmetric operators
\begin{align}
b_1=c_1&=\frac{1}{2}rv_x^2,\\
b_2=c_2&=\frac{1}{2}rv^2.
\end{align}
Other coefficients can be expressed in terms of $(r,v_x,v)$ as
\begin{align}
(a_1,d_1)&=\frac{1}{2}v_x^2\left(1\pm\sqrt{1-r^2}\right),\\
(a_2,d_2)&=\frac{1}{2}v^2\left(1\mp\sqrt{1-r^2}\right).
\end{align}
Substitute the above expressions into the last constraint \ref{eq:abcd5} and solve for 
\begin{equation}
r=\frac{v_n}{v_x}.
\end{equation} 
\par
To compute the gradients, define new variables
\begin{align}
\alpha&=\sqrt{2(\epsilon-\delta}),\\
\beta&=\sqrt{1+2\epsilon},\\
\gamma&=\sqrt{\beta^2-\alpha^2},
\end{align}
so that
\begin{equation}
1+2\delta=\gamma^2,v_x=v\beta,v_n=v\gamma,r=\frac{\gamma}{\beta},\sqrt{1-r^2}=\frac{\alpha}{\beta},
\end{equation}
and
\begin{align}
(a_1,d_1)&=\frac{1}{2}v^2\beta\left(\beta\pm\alpha\right),\\
(a_2,d_2)&=\frac{1}{2}v^2\left(1\mp\frac{\alpha}{\beta}\right),\\
b_1=c_1&=\frac{1}{2}v^2\beta\gamma,\\
b_2=c_2&=\frac{1}{2}v^2\frac{\gamma}{\beta}.
\end{align}
Now derivatives with respect to $(v,\alpha,\beta)$ can be computed from chain rule
\begin{align}
\begin{split}
\frac{\partial\chi}{\partial v}{}&=v\left\{\frac{\partial\chi}{\partial a_1}\beta(\beta+\alpha)+\frac{\partial\chi}{\partial d_1}\beta(\beta-\alpha)+\frac{\partial\chi}{\partial a_2}\left(1-\frac{\alpha}{\beta}\right)+\frac{\partial\chi}{\partial d_2}\left(1+\frac{\alpha}{\beta}\right)\right.\\
&\left.+\gamma\left[\left(\frac{\partial\chi}{\partial b_1}+\frac{\partial\chi}{\partial c_1}\right)\beta+\left(\frac{\partial\chi}{\partial b_2}+\frac{\partial\chi}{\partial c_2}\right)\frac{1}{\beta}\right]\right\},
\end{split}
\\
\begin{split}
\frac{\partial\chi}{\partial \alpha}{}&=\frac{1}{2}v^2\left\{\left(\frac{\partial\chi}{\partial a_1}-\frac{\partial\chi}{\partial d_1}\right)\beta-\left(\frac{\partial\chi}{\partial a_2}-\frac{\partial\chi}{\partial d_2}\right)\frac{1}{\beta}\right.\\
&\left.-\frac{\alpha}{\gamma}\left[\left(\frac{\partial\chi}{\partial b_1}+\frac{\partial\chi}{\partial c_1}\right)\beta+\left(\frac{\partial\chi}{\partial b_2}+\frac{\partial\chi}{\partial c_2}\right)\frac{1}{\beta}\right]\right\},
\end{split}
\\
\begin{split}
\frac{\partial\chi}{\partial \beta}{}&=\frac{1}{2}v^2\left\{\left(\frac{\partial\chi}{\partial a_1}-\frac{\partial\chi}{\partial d_1}\right)(2\beta+\alpha)+\left(\frac{\partial\chi}{\partial a_2}-\frac{\partial\chi}{\partial d_2}\right)\frac{\alpha}{\beta^2}\right.\\
&\left.-\frac{1}{\gamma}\left[\left(\frac{\partial\chi}{\partial b_1}+\frac{\partial\chi}{\partial c_1}\right)(2\beta^2-\alpha^2)+\left(\frac{\partial\chi}{\partial b_2}+\frac{\partial\chi}{\partial c_2}\right)\frac{\alpha^2}{\beta^2}\right]\right\},
\end{split}
\end{align}
After the inversion, $(\epsilon,\delta)$ can be retrieved from $(\alpha,\beta)$ by
\begin{align}
\epsilon&=\frac{\beta^2-1}{2},\\
\delta&=\epsilon-\frac{\alpha^2}{2}.
\end{align}

\newpage
\section{Summary}
\begin{tcolorbox}[colframe=blue!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\sigma=CD\sigma,\\
\begin{cases}
\partial_t^2\sigma_x=c_{11}(\partial_x^2+\partial_y^2)\sigma_x+c_{13}\partial_z^2\sigma_z,\\
\partial_t^2\sigma_z=c_{13}(\partial_x^2+\partial_y^2)\sigma_x+c_{33}\partial_z^2\sigma_z,
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}

\begin{tcolorbox}[colframe=red!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\lambda=DC\sigma,\\
\begin{cases}
\partial_t^2\lambda_x=(\partial_x^2+\partial_y^2)(c_{11}\lambda_x+c_{13}\lambda_z),\\
\partial_t^2\lambda_z=\partial_z^2(c_{13}\lambda_x+c_{33}\lambda_z),
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}

\begin{tcolorbox}[colframe=blue!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\sigma=RDR\sigma,\\
\begin{cases}
\partial_t^2\sigma_x=r_{11}(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+r_{13}\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z),\\
\partial_t^2\sigma_z=r_{13}(\partial_x^2+\partial_y^2)(r_{11}\sigma_x+r_{13}\sigma_z)+r_{33}\partial_z^2(r_{13}\sigma_x+r_{33}\sigma_z).
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}

\begin{tcolorbox}[colframe=red!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\sigma=D_1CD_1\sigma,\\
\begin{cases}
\partial_t^2\sigma_x=\partial_x(c_{11}\partial_x\sigma_x+c_{11}\partial_y\sigma_y+c_{13}\partial_z\sigma_z),\\
\partial_t^2\sigma_y=\partial_y(c_{11}\partial_x\sigma_x+c_{11}\partial_y\sigma_y+c_{13}\partial_z\sigma_z),\\
\partial_t^2\sigma_z=\partial_z(c_{13}\partial_x\sigma_x+c_{13}\partial_y\sigma_y+c_{33}\partial_z\sigma_z).
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}

\begin{tcolorbox}[colframe=blue!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\sigma=\begin{bmatrix} A & B \\ C & D \end{bmatrix}\sigma,\\
\begin{cases}
\partial_t^2\sigma_x=\left[a_1(\partial_x^2+\partial_y^2)+a_2\partial_z^2\right]\sigma_x+\left[b_1(\partial_x^2+\partial_y^2)+b_2\partial_z^2\right]\sigma_z,\\
\partial_t^2\sigma_z=\left[c_1(\partial_x^2+\partial_y^2)+c_2\partial_z^2\right]\sigma_x+\left[d_1(\partial_x^2+\partial_y^2)+d_2\partial_z^2\right]\sigma_z,
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}

\begin{tcolorbox}[colframe=blue!50!white]
\begin{equation}
\begin{gathered}
\partial_t^2\lambda=\begin{bmatrix} A^T & C^T \\ B^T & D^T \end{bmatrix}\lambda,\\
\begin{cases}
\partial_t^2\lambda_x=(\partial_x^2+\partial_y^2)(a_1\lambda_x+c_1\lambda_z)+\partial_z^2(a_2\lambda_x+c_2\lambda_z),\\
\partial_t^2\lambda_z=(\partial_x^2+\partial_y^2)(b_1\lambda_x+d_1\lambda_z)+\partial_z^2(b_2\lambda_x+d_2\lambda_z).
\end{cases}
\end{gathered}
\end{equation}
\end{tcolorbox}


\end{document}  
